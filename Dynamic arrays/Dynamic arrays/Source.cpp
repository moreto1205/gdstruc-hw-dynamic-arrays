#include <iostream>
#include<string>
#include<vector>
#include"Arrays.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	int size = 0;
	int choice = 0;
	cout << "ENTER SIZE OF ARRAY" << endl;
	cin >> size;

	UnorderedArray<int> grades(size);
	
	for (int i = 1; i <= size; i++)
	{
		int randNums = rand() % 100 + 1;
		grades.push(randNums);
	}
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	cout << " PLEASE REMOVE AN ELEMENT " << endl;
	cin >> choice;

	grades.remove(choice);
	grades.pop();

	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	cout << " PLEASE SEARCH AN ELEMENT" << endl;
	cin >> choice;

	for (int i = 0; i < grades.getSize(); i++)
	{
		if (choice == grades[i])
		{
			cout << " ELEMENT HAS BEEN FOUND" << endl;
			cout << grades[i] << endl;
		}

		if (i > grades.getSize())
		{
			cout << "ELEMENT IS NOT FOUND" << endl;
		}
	}


	system("pause");
}